<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Uploader extends Controller {

	public function action_index()
	{
		$view = View::factory('uploader/index');
	    $this->response->body($view);
	}

	public function action_ajax_load_image_upload_list()
	{
		$view = View::factory('uploader/ajax_load_image_upload_list');

		$uploads = ORM::factory('Upload')->find_all();
	    $view->uploads = $uploads;
	 
	    // The view will have $places and $user variables
	    $this->response->body($view);
	}

	public function action_add()
	{
		$filename = NULL;
		$return['is_success'] = false;
		$return['message'] = '<div class="alert alert-danger">Unable to upload file.</div>';

		if ($this->request->method() == Request::POST)
        {
            if (isset($_FILES['image_file']))
            {
                $filename = $this->_save_image($_FILES['image_file']);

                if ($filename !== FALSE) 
                {
                	$uploads = ORM::factory('Upload');
                	$uploads->title = $_POST['title'];
                	$uploads->thumbnail = "thumb_".$filename;
                	$uploads->filename = $filename;
                	$uploads->date_added = date("Y-m-d H:i:s");
                	$uploads->save();

                	$return['is_success'] = true;
					$return['message'] = '<div class="alert alert-success">You have successfully added the file.</div>';
                }
            }
        }
 
        echo json_encode($return);
	}

	public function action_update()
	{
		$filename = NULL;
		$return['is_success'] = false;
		$return['message'] = '<div class="alert alert-danger">Unable to upload file.</div>';

		if ($this->request->method() == Request::POST)
        {
        	$upload = ORM::factory('Upload', $_POST['id']);

        	if ($upload->loaded())
			{
			    if (isset($_FILES['image_file']))
            	{
            		$filename = $this->_save_image($_FILES['image_file']);
            		if ($filename !== FALSE) 
                	{
                		$directory = str_replace('\\',"/",DOCROOT).'uploads/'.$upload->filename;
			        	if(file_exists($directory))
			        		unlink($directory);
			        	
			        	$directory_thumb = str_replace('\\',"/",DOCROOT).'uploads/thumbnail/thumb_'.$upload->filename;
			        	if(file_exists($directory_thumb))
			        		unlink($directory_thumb);
			        	
                		$upload->thumbnail = "thumb_".$filename;
                		$upload->filename = $filename;
                	}
            	}

			    $upload->title = $_POST['title'];
			    $upload->save();

			    $return['is_success'] = true;
				$return['message'] = '<div class="alert alert-success">Record has been successfully updated.</div>';
			}
        }
 
        echo json_encode($return);
	}

	protected function _save_image($image)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }
 
        $directory = DOCROOT.'uploads/';
        $directory_thumb = DOCROOT.'uploads/thumbnail/';
 
        if ($file = Upload::save($image, NULL, $directory))
        {
            $filename = date("YmdHis").'.jpg';
 
            Image::factory($file)
                ->resize(400, 400, Image::AUTO)
                ->save($directory.$filename);

            Image::factory($file)
                ->resize(100, 100, Image::AUTO)
                ->save($directory_thumb."thumb_".$filename);
 
            // Delete the temporary file
            unlink($file);
 
            return $filename;
        }
 
        return FALSE;
    }

    public function action_delete() 
    {
    	$return['is_success'] = false;
    	if ($this->request->method() == Request::POST)
        {
        	$upload = ORM::factory('Upload', $_POST['id']);

        	$directory = str_replace('\\',"/",DOCROOT).'uploads/'.$upload->filename;
        	if(file_exists($directory))
        		unlink($directory);
        	
        	$directory_thumb = str_replace('\\',"/",DOCROOT).'uploads/thumbnail/thumb_'.$upload->filename;
        	if(file_exists($directory_thumb))
        		unlink($directory_thumb);

        	$upload->delete();
        	$return['is_success'] = true;
        }
        echo json_encode($return);
    }

} //
