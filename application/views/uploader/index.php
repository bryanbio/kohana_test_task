<?php echo View::factory('template/header'); ?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Image Uploader </h1> 
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#uploadFileModal" title="Upload Image" ><i class="fa fa-upload"></i> Upload Image</button>
                </div>
                <!-- /.panel-heading -->
                <div id="image-upload-container" class="dataTable_wrapper" style="padding-top:10px;">                                
                    
                </div>
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

<div class="modal fade" id="uploadFileModal" tabindex="-1" role="dialog" aria-labelledby="uploadFileModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="uploadFileModalLabel">Upload Image</h4>
            </div>
            <form id="frm-upload" onsubmit="return false;"  role="form" method="post" enctype="multipart/form-data" >
                <div class="modal-body">
                    <fieldset>  
                    	<div class='form-group'>
                    		<div class='col-sm-12 form-label'>Title</div>
                    		<div class='col-sm-12'>
                    			<input type="text" class="form-control" name="title" id="title" required>
                    			<div class='help-block with-errors'></div>
                    		</div>
                    	</div>
                    	<div class='form-group'>
                    		<div class='col-sm-12 form-label'>Image</div>
                    		<div class='col-sm-12'>
                    			<div class="input-group ">
                    				<input type="file"  class="hidden" name="image_file" id="file" required>
					                <input type="text" class="form-control" id="filename" name="filename" readonly="" required>
					                    <span class="input-group-btn">
					                      <button class="btn btn-info btn-flat btn-browse" type="button">Browse...</button>
					                    </span>
					            </div>
                    			<div class='help-block with-errors'></div>
                    		</div>
                    	</div> 
                        <div class='form-group'>
                            <div class='col-sm-12 '><div id="msg_container"></div></div>
                        </div> 
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-upload-file">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
var BASE_URL = '<?php echo url::base(); ?>';
</script>

<?php echo View::factory('template/footer'); ?>