<table class="table table-striped table-bordered table-hover" id="dataTables-list">
    <thead>
        <tr>
            <th>Title</th>
            <th>Thumbnail</th>
            <th>Filename</th>
            <th>Date added</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($uploads as $u) { ?>
            <tr>
                <td><?= $u->title; ?></td>
                <td align="center"><img src="<?= 'uploads/thumbnail/'.$u->thumbnail; ?>" /></td>
                <td><?= $u->filename; ?></td>
                <td><?= date("M d, Y", strtotime($u->date_added)); ?></td>
                <td>
                    <div class="text-center">
                        <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#updateModal-<?= $u->id ?>" ><i class="fa fa-pencil"></i></button>
                        <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#deleteUploadModal-<?= $u->id ?>" ><i class="fa fa-trash"></i></button>
                    </div>

                    <div class="modal fade" id="updateModal-<?= $u->id; ?>" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel">
                        <div class="modal-dialog " role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="updateModalLabel">Edit Image</h4>
                                </div>
                                <form id="frm-edit-<?= $u->id; ?>" onsubmit="return false;"  role="form" method="post" enctype="multipart/form-data" >
                                    <input type="hidden" name="id" value="<?= $u->id;?>" >
                                    <div class="modal-body">
                                        <fieldset>  
                                            <div class='form-group'>
                                                <div class='col-sm-12 form-label'>Title</div>
                                                <div class='col-sm-12'>
                                                    <input type="text" class="form-control" name="title" id="title" value="<?= $u->title ?>" required>
                                                    <div class='help-block with-errors'></div>
                                                </div>
                                            </div>
                                            <div class='form-group'>
                                                <div class='col-sm-12 form-label'>Image</div>
                                                <div class='col-sm-12'>
                                                    <div class="input-group ">
                                                        <input type="file"  class="hidden file-img" name="image_file" data-id="<?= $u->id; ?>" id="file-<?= $u->id; ?>" required>
                                                        <input type="text" class="form-control" data-id="<?= $u->id; ?>" id="filename-<?= $u->id; ?>" name="filename" readonly="" value="<?= $u->filename; ?>" required>
                                                            <span class="input-group-btn">
                                                              <button class="btn btn-info btn-flat btn-browse" data-id="<?= $u->id; ?>" type="button">Browse...</button>
                                                            </span>
                                                    </div>
                                                    <div class='help-block with-errors'></div>
                                                </div>
                                            </div> 
                                            <div class='form-group'>
                                                <div class='col-sm-12 '><img src="<?= 'uploads/thumbnail/'.$u->thumbnail; ?>" /></div>
                                            </div>
                                            <div class='form-group'>
                                                <div class='col-sm-12 '><div id="msg_container_<?= $u->id; ?>"></div></div>
                                            </div> 
                                        </fieldset>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" data-id="<?= $u->id; ?>" class="btn btn-primary btn-edit-upload-file">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="deleteUploadModal-<?= $u->id ?>" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel-<?= $u->id ?>">
                        <div class="modal-dialog " role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="deleteModalLabel-<?= $u->id ?>">Delete Image</h4>
                              </div>

                              <div class="modal-body">
                                <p>Are you sure you want to delete selected entry?</p>
                              </div>
                              <div class="modal-footer">
                                <button type="button" data-id="<?= $u->id; ?>" class="btn btn-danger btn-delete-image" >Yes</button>                              
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<script>
var BASE_URL = '<?php echo url::base(); ?>';
$(function(){
    $('#dataTables-list').DataTable({
        responsive: true
    });

    $('.btn-delete-image').click(function(){
        var id = $(this).attr("data-id");
        var obj_btn = $(this);
        obj_btn.html('Deleting...');
        $.post(BASE_URL + "index.php/uploader/delete",{id:id},function(o) {
            if(o.is_success) {
                $("#deleteUploadModal-"+id).modal('toggle');
                setTimeout(function(){
                  load_image_upload_list_dt();
                },1000);
            }else{

            }
            obj_btn.html("Yes");
        },"json");
    });

    $('.btn-edit-upload-file').click(function(){
        var id = $(this).attr("data-id");
        var formData = new FormData($('#frm-edit-'+id)[0]);
        $.ajax({
            url: BASE_URL + "index.php/uploader/update",  //Server script to process data
            type: "POST",
            data : formData,
            processData: false,
            contentType: false,
            success:function(data, textStatus, jqXHR){
                var o = JSON.parse(data);
                $('#msg_container-'+id).html(o.message);
                if(o.is_success){
                    $("#updateModal-"+id).modal('toggle');
                    setTimeout(function(){
                      load_image_upload_list_dt();
                    },1000);
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                //if fails     
            }
        });

    });

    $('.btn-browse').click(function(){
        var id = $(this).attr("data-id");
        $('#file-'+id).trigger('click');
    });

    $('.file-img').change(function(){
        var id = $(this).attr("data-id");
        //console.log($(this));
        $('#filename-'+id).val($(this).val());
    });
});
</script>