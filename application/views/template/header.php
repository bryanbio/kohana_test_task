<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Uploader</title>

    <!-- Bootstrap Core CSS -->
    <link href="system/media/css/bootstrap.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="system/media/css/datatable/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="system/media/css/app.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="system/media/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>
	<div id="wrapper">
	<!-- followed by page content and footer -->