
</div><!-- /#wrapper -->
<!-- jQuery -->
<script src="system/media/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="system/media/js/bootstrap/bootstrap.min.js"></script>

<!-- DataTables JavaScript -->
<script src="system/media/js/datatable/jquery.dataTables.min.js"></script>
<script src="system/media/js/datatable/dataTables.bootstrap.min.js"></script>

<script>
$(function(){
    $('.btn-browse').click(function(){
        $('#file').trigger('click');
    });

    $('#file').change(function(){
        //console.log($(this));
        $('#filename').val($(this).val());
    });

	$('.btn-upload-file').click(function(){
		var formData = new FormData($('#frm-upload')[0]);
	    $.ajax({
	        url: BASE_URL + "index.php/uploader/add",  //Server script to process data
		    type: "POST",
		    data : formData,
		    processData: false,
		    contentType: false,
		    success:function(data, textStatus, jqXHR){
		        var o = JSON.parse(data);
		        $('#msg_container').html(o.message);
		        if(o.is_success){
		        	load_image_upload_list_dt()
		        }
		    },
		    error: function(jqXHR, textStatus, errorThrown){
		        //if fails     
		    }
	    });

	});

	load_image_upload_list_dt();

});

function load_image_upload_list_dt() {
    $("#image-upload-container").html('<div class="text-center" style="padding:20px;"><i class="fa fa-spin fa-spinner" ></i> Loading content...</div>');  
    $.get(BASE_URL + "index.php/uploader/ajax_load_image_upload_list",{},function(o) {
      	$('#image-upload-container').html(o);    
    }); 
}
</script>

</body>

</html>